//
//  Utils.swift
//  SceneStuff
//
//  Created by Markus Kruusmägi on 2018-11-09.
//  Copyright © 2018 Gravelhill. All rights reserved.
//

import Foundation
import SceneKit
import GameplayKit
import simd

extension SCNVector3 {

    var random: SCNVector3 {
        return SCNVector3Zero
    }

    public static func randomInCube() -> SCNVector3 {
        let x = CGFloat.random(in: 0.5...1.0)
        let y = CGFloat.random(in: 0.5...1.0)
        let z = CGFloat.random(in: 0.5...1.0)
        return SCNVector3(x, -1.0, z)

    }
    public static func random(_ radius: Float = 1.0) -> SCNVector3 {
        let random = GKRandomSource()

        let source = GKGaussianDistribution(randomSource: random, mean: 0.0, deviation: 1000.0)

        let x = source.nextUniform()/1000.0
        let y = source.nextUniform()/1000.0
        let z = source.nextUniform()/1000.0

        let k = radius / sqrt(x*x + y*y + z*z)
        print("x \(x) y \(y) z \(z) k: ", k)
        return SCNVector3(x: x*k, y: y*k, z: z*k)
    }

}

extension SCNQuaternion {
    public func length() -> Float {
        let b = (x * x) + (y * y) + (z * z) + (w * w)
        return sqrt(b)
    }

    public func normalized() -> SCNQuaternion {
        let l = length()
        return SCNQuaternion(x: x / l, y: y / l, z: z / l, w: w / l)
    }
}

extension SCNVector3 {
    func absolute() -> SCNVector3 {
        return SCNVector3(abs(x), abs(y), abs(z))
    }

    var largestComponentIndex: Int {
        if x >= y && x >= z {
            return 0
        } else if y > z {
            return 1
        }
        return 2
    }
}

extension SCNVector3 {

    static prefix func -(vec: SCNVector3) -> SCNVector3 {
        return SCNVector3(-vec.x, -vec.y, -vec.z)
    }

    func dot(_ other: SCNVector3) -> Float {
        return self.x * other.x + self.y * other.y + self.z * other.z
    }

    func cross(_ other: SCNVector3) -> SCNVector3 {
        return SCNVector3(simd.cross(float3(self), float3(other)))
    }

    static func +(lhs: SCNVector3, rhs: SCNVector3) -> SCNVector3 {
        return SCNVector3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z)
    }

    static func -(lhs: SCNVector3, rhs: SCNVector3) -> SCNVector3 {
        return SCNVector3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z)
    }


    static func /(lhs: SCNVector3, s: Float) -> SCNVector3 {
        return SCNVector3(lhs.x / s, lhs.y / s, lhs.z / s)
    }

    static func *(lhs: SCNVector3, s: Float) -> SCNVector3 {
        return SCNVector3(lhs.x * s, lhs.y * s, lhs.z * s)
    }

    static func /(lhs: SCNVector3, s: Int) -> SCNVector3 {
        return lhs / Float(s)
    }

    public func length() -> Float {
        let b = (x * x) + (y * y) + (z * z)
        return sqrt(b)
    }

    public func sqrLength() -> Float {
        return (x * x) + (y * y) + (z * z)
    }

    public func normalized() -> SCNVector3 {
        let l = length()
        return SCNVector3(x: x / l, y: y / l, z: z / l)
    }

}

extension SCNMatrix4 {
    var right: SCNVector3 {
        return SCNVector3(x: m11, y: m12, z: m13)
    }
    var up: SCNVector3 {
        return SCNVector3(x: m21, y: m22, z: m23)
    }
    var forward: SCNVector3 {
        return SCNVector3(x: m31, y: m32, z: m33)
    }

    func faceUp() -> Int {

        let worldUp = SCNVector3(0, 1, 0)

        let faces:[SCNVector3] = [up, -up, right, -right, forward, -forward]

        var epsilon = Float.infinity
        var found = -1

        for (index, face) in faces.enumerated() {
            let e = 1.0 - face.dot(worldUp)
            if e < epsilon {
                epsilon = e
                found = index
            }
        }

        return found
    }

}

extension Array where Element == SCNVector3 {
    func average() -> SCNVector3 {

        let sum = self.reduce(SCNVector3Zero) { (sum, vec) in
            return sum + vec
        }
        return sum / self.count
    }
}

extension UIColor {

    static func randomHSV() -> UIColor {

        let hue = CGFloat.random(in: 0.0...1.0)
        return UIColor.init(hue: hue, saturation: 0.9, brightness: 0.4, alpha: 1.0)

    }
}
