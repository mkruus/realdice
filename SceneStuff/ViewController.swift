//
//  ViewController.swift
//  SceneStuff
//
//  Created by Markus Kruusmägi on 2018-11-03.
//  Copyright © 2018 Gravelhill. All rights reserved.
//

import UIKit
import SceneKit
import SpriteKit
import simd

class ViewController: UIViewController {

    private let statPoster = StatPoster()
    private var initialRoll: Bool = false

    @IBOutlet var sceneView: SCNView! {
        willSet {
            newValue.allowsCameraControl = false
            newValue.showsStatistics = false
            newValue.autoenablesDefaultLighting = true
        }
    }

    let diceFaces: [Int] = [2, 5, 3, 4, 1, 6]

    lazy var scene: SCNScene = {
        guard let scene = SCNScene(named: "scene.scnassets/scene.scn") else {
            fatalError("Could not load scene file")
        }
        return scene
    }()

    func setupObjects() {
    }
    private var shouldMove = true

    lazy var dice: SCNNode = {
        return scene.rootNode.childNode(withName: "dice", recursively: false)!
    }()

    lazy var dice2: SCNNode = {
        let node = dice.clone()
        return node
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        sceneView.delegate = self
        sceneView.scene = scene
        setupObjects()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(sender:)))

        sceneView.addGestureRecognizer(tapGesture)
        let camera = scene.rootNode.childNode(withName: "camera", recursively: true)!

        camera.camera?.zFar = 1000
//        print(dice.worldOrientation)
//        print(dice.presentation.worldOrientation)

        scene.rootNode.addChildNode(dice2)

        let overlay = SKScene(size: sceneView.bounds.size)
        let label = SKLabelNode.init(text: "")
        label.name = "label"
        label.fontName = "Courier-Bold"
        label.fontSize = 50

        label.fontColor = .red
        label.position = CGPoint(x: 75, y: overlay.size.height - 55)
        let shape = SKShapeNode.init(rectOf: CGSize(width: 130, height: 56), cornerRadius: 10.0)
        shape.position = CGPoint(x: 75, y: overlay.size.height - 38)
        shape.lineWidth = 2.0
        shape.fillColor = UIColor.white.withAlphaComponent(0.75)

        
        overlay.addChild(shape)
        overlay.addChild(label)
        sceneView.overlaySKScene = overlay

//        let lookat = SCNLookAtConstraint(target: dice.presentation)
//        camera.constraints = [lookat]
//        sceneView.pointOfView?.constraints = [lookat]
        sceneView.pointOfView = camera
    }

    @objc func didTapView(sender: UITapGestureRecognizer) {
        let color = UIColor.randomHSV()
        print(color)

        updateLabel(text: "", color: color)
        dice.geometry?.firstMaterial?.diffuse.contents = color

        applyRandomThrowTo(dice)
        applyRandomThrowTo(dice2)

        shouldMove = true

    }

    func applyRandomThrowTo(_ node: SCNNode) {
        let dirPos = randomDirAndPos()
        node.physicsBody?.applyForce(dirPos.0, at: dirPos.1, asImpulse: true)

        let torque = randomTorque()
        node.physicsBody?.applyTorque(torque, asImpulse: true)
    }

    func randomTorque() -> SCNVector4 {
        let torqueAxis = SCNVector3.random(2.0)
        let torqueAmount = Float.random(in: -2.0...2.0)
        return SCNVector4(torqueAxis.x, torqueAxis.y, torqueAxis.z, torqueAmount)
    }

    func randomDirAndPos() -> (SCNVector3, SCNVector3) {
        let xDir = Float.random(in: -3...3)
        let zDir = Float.random(in: -3...3)
        let dir = SCNVector3(xDir, 3, zDir)
        let pos = SCNVector3.randomInCube()
        return (dir, pos)
    }

    private func updateLabel(text: String?, color: UIColor?) {
        if let label = sceneView.overlaySKScene?.childNode(withName:  "label") as? SKLabelNode {
            label.text = text
            if let color = color {
                label.fontColor = color
            }
        }
    }
}

extension ViewController: SCNSceneRendererDelegate {

    func updateCamera() {
        let camera = scene.rootNode.childNode(withName: "camera", recursively: true)!
        let light = scene.rootNode.childNode(withName: "omni", recursively: true)!

        let middle = [dice.presentation.position, dice2.presentation.position].average()

        let fov = Float(camera.camera?.fieldOfView ?? 60) * Float.pi / 180
        let distVec = dice2.presentation.position - dice.presentation.position
        let dist = distVec.length()

        let cameradist = (dist * 0.5) / tan(fov / 2)

        var dir = SCNVector3(0, 1, 0).cross(distVec.normalized())
        dir = dir * abs(cameradist) * 2
//        let quat = simd_quatf(angle: Float.pi/4, axis: float3(distVec.normalized()))
        let axis = distVec.normalized()
//        let scnQuat = SCNQuaternion(quat.vector.x,quat.vector.y,quat.vector.z,quat.vector.w)
//        let mat = simd_matrix3x3(quat)
        let scnMat = SCNMatrix4MakeRotation(Float.pi/3, axis.x, axis.y, axis.z)

        let mat = float4x4(scnMat)
        let b = mat * float4.init(x: dir.x, y: dir.y, z: dir.z, w: 1)
        let newDir = SCNVector3(b.x,b.y,b.z) + middle

        camera.position = newDir

        camera.look(at: middle, up: SCNVector3(0, 1, 0), localFront: SCNVector3(0, 0.1, -1))

        light.position = middle + SCNVector3(0,6,0)

//        print("Fov: \(fov)")
//        print("dist: \(dist)")
//        print("dir: \(dir)")
//        print("middle: \(middle)")
//        print("Cameradist: \(cameradist)")
//        print("Camera pos: \(camera.position)")
//        print("Camera euler: \(camera.eulerAngles)")
    }
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {


//        print("Update \(time)")
//        let camera = scene.rootNode.childNode(withName: "camera", recursively: true)!
//        let dice = scene.rootNode.childNode(withName: "dice", recursively: false)!
//        let light = scene.rootNode.childNode(withName: "omni", recursively: true)!

//        print(dice.presentation.position)

//        let position = SCNVector3(dice.presentation.position.x, dice.presentation.position.y + 5, dice.presentation.position.z - 5)
//        let at = dice.presentation.position
//
//        let middle = [dice.presentation.position, dice2.presentation.position].average()

//        camera.position = middle + SCNVector3(0, 5, -5)
//        camera.look(at: middle)
//        light.position = camera.position
        updateCamera()

    }


    func renderer(_ renderer: SCNSceneRenderer, didSimulatePhysicsAtTime time: TimeInterval) {
        let dice = scene.rootNode.childNode(withName: "dice", recursively: false)!

        if dice.physicsBody?.isResting == true && dice2.physicsBody?.isResting == true && shouldMove {
            print("Resting!")
//            print(dice.presentation.worldOrientation.normalized())
            let diceFace = diceFaces[dice.presentation.transform.faceUp()]
            let diceFace2 = diceFaces[dice2.presentation.transform.faceUp()]
            print("Result: ", diceFace, diceFace2)
            if initialRoll {
                let stat = Stat.init(data: diceFace, date: Date(), id: nil)
                let stat2 = Stat.init(data: diceFace2, date: Date(), id: nil)
                statPoster.postStat(stat)
                statPoster.postStat(stat2)
            }
            initialRoll = true

            updateLabel(text: "\(diceFace2) \(diceFace)", color: nil)
//            dice2.position = dice.position + SCNVector3(0.5, 0, 0)
            updateCamera()
            let middle = (dice2.presentation.position + dice.presentation.position) / 2

            let newPos1 = (dice.presentation.position - middle).normalized() * 1 + middle
            let newPos2 = (dice2.presentation.position - middle).normalized() * 1 + middle

            dice2.position = dice2.presentation.position
            dice2.rotation = dice2.presentation.rotation
            dice.position = dice.presentation.position
            dice.rotation = dice.presentation.rotation

            dice.runAction(SCNAction.move(to: newPos1, duration: 0.25))
            dice2.runAction(SCNAction.move(to: newPos2, duration: 0.25))

            shouldMove = false
        }

    }
}

