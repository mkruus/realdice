//
//  StatPoster.swift
//  SceneStuff
//
//  Created by Markus Kruusmägi on 2019-01-01.
//  Copyright © 2019 Gravelhill. All rights reserved.
//

import Foundation

struct Stat: Codable {
    let data: Int
    let date: Date
    let id: Int?
}

class StatPoster {
    private let urlSession: URLSession

    func postStat(_ stat: Stat) {

        let url = URL(string: "http://192.168.1.100:8080/stats")!

        let request: URLRequest = {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let data = try? JSONEncoder().encode(stat)
            request.httpBody = data
            return request
        }()

        let task = urlSession.dataTask(with: request) { (data, response, error) in
            if let response = response as? HTTPURLResponse {
                print("STATUS: \(response.statusCode)")
            }
            if let data = data {
                let dataText = String.init(data: data, encoding: String.Encoding.utf8) ?? "-"
                print("DATA: \(dataText)")
            }
            if let error = error {
                print("ERROR: \(error.localizedDescription)")
            }
        }
        task.resume()
    }

    init(session: URLSession = URLSession.shared) {
        self.urlSession = session
    }

}
